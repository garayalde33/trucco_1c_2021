########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

####Ejemplo 7: 
#PROYECTO_ACTIVO = 7_proyecto1
#NOMBRE_EJECUTABLE = 7_proyecto1.exe

####Ejemplo 9: 
#PROYECTO_ACTIVO = 9_proyecto1
#NOMBRE_EJECUTABLE = 9_proyecto1.exe

####Ejemplo 12: 
#PROYECTO_ACTIVO = 12_proyecto1
#NOMBRE_EJECUTABLE = 12_proyecto1.exe

####Ejemplo 14: 
#PROYECTO_ACTIVO = 14_proyecto1
#NOMBRE_EJECUTABLE = 14_proyecto1.exe

####Ejemplo 16: 
#PROYECTO_ACTIVO = 16_proyecto1
#NOMBRE_EJECUTABLE = 16_proyecto1.exe

####Ejemplo 17:
PROYECTO_ACTIVO=17_proyecto1
NOMBRE_EJECUTABLE = 17_proyecto1.exe

####Ejemplo Integrador:
#PROYECTO_ACTIVO = integrador_a_proyecto1
#NOMBRE_EJECUTABLE = integrador_a_proyecto1.exe

####Ejemplo Integrador:
#PROYECTO_ACTIVO = integrador_c_proyecto1
#NOMBRE_EJECUTABLE = integrador_c_proyecto1.exe

####Ejemplo Integrador:
#PROYECTO_ACTIVO = integrador_d_proyecto1
#NOMBRE_EJECUTABLE = integrador_d_proyecto1.exe