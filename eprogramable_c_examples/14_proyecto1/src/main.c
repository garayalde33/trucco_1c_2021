/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero
(usando acceso por punteros). */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stdlib.h"
#include "time.h"

/*==================[macros and definitions]=================================*/

 typedef struct {
    	uint8_t apellido[20];
    	uint8_t nombre[12];
    	uint8_t edad;

    } alumno;
    alumno alumno_1;
    alumno *alum;
    alumno alumno_2;
    uint8_t ape[] = "Trucco";
    uint8_t nom[] = "Teresita";
    uint8_t ape2[] = "Diaz";
    uint8_t nom2[] = "Mel";

/*==================[internal functions declaration]=========================*/

int main(void)
{
	alum = &alumno_2;

  alumno_1.edad = 25;




  uint8_t i;
  printf("Edad alumno 1: %d\r\n", alumno_1.edad);

  for(i=0 ; i<20 ; i++){

	  alumno_1.apellido[i]=ape[i];
	  alumno_1.nombre[i]=nom[i];

  }

  printf("Apellido alumno 1: %s \r\n", alumno_1.apellido);
  printf("Nombre alumno 1: %s \r\n", alumno_1.nombre);


  alum->edad = 23;
  uint8_t j;
   printf("Edad alumno 1: %d\r\n", alumno_2.edad);

   for(j=0 ; j<20 ; j++){

 	  alumno_2.apellido[j]=ape2[j];
 	  alumno_2.nombre[j]=nom2[j];

   }

   printf("Apellido alumno 1: %s \r\n", alumno_2.apellido);
   printf("Nombre alumno 1: %s \r\n", alumno_2.nombre);




	return 0;
}

/*==================[end of file]============================================*/

